package programmablethermostat;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.*;
import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;

/**
 *
 * @author tguar
 */
public class ReadWriteProgramming {
    
    /**
     * reads from a CSV of saved thermostat settings, returns java objects
     * @param settingsPath
     * @return ArrayList of ProgrammedSettings objects
     */
    public static ArrayList readProgramming(Path settingsPath) {
        // ArrayList to hold all programmings
        ArrayList<ProgrammedSettings> programs = new ArrayList<>();
        
        // Check for existence of file
        if(Files.notExists(settingsPath)) {
            programs = null;
        }
        // If file exists, read data into objects into list
        else {
            try {
                Scanner dataReader = new Scanner(settingsPath);
                while(dataReader.hasNextLine()) {
                    String settings[] = dataReader.nextLine().trim().split(",");
                    programs.add(
                            new ProgrammedSettings(
                                    settings[0], 
                                    LocalTime.parse(settings[1]),
                                    Integer.parseInt(settings[2])
                            )
                    );
                }
            } 
            catch (FileNotFoundException e) {
                ProgrammableThermostat.LOGGER.log(Level.SEVERE, "Files.exists is true, but File not found on read: " + e.getMessage());
                programs = null;
            }
            catch (IOException e) {
                ProgrammableThermostat.LOGGER.log(Level.SEVERE, "IO Error saving file: " + e.getMessage());
                programs = null;
            }
            catch (NumberFormatException e) {
                ProgrammableThermostat.LOGGER.log(Level.SEVERE, "Corrupt data - Incorrect number format: " + e.getMessage());
                programs = null;
            }
            catch (Exception e) {
                ProgrammableThermostat.LOGGER.log(Level.SEVERE, e.getMessage());
                programs = null;
            }
        }
        
        return programs;
    }
    
    /**
     * Saves thermostat programming settings to file in CSV format
     * @param settingsPath: path to file
     * @param programs: ArrayList of thermostat programming objects
     * @return 
     */
    public static boolean saveProgramming(Path settingsPath, ArrayList<ProgrammedSettings> programs) {

        boolean success;
        
        try (OutputStream out = new BufferedOutputStream(
        Files.newOutputStream(settingsPath, CREATE, TRUNCATE_EXISTING))) {
            if(programs != null) {
                for (ProgrammedSettings setting : programs) {
                    String s = setting.getWeekday() + "," + setting.getStartTime().toString() + 
                            "," + setting.getTargetTemp() + "\n";
                    byte data[] = s.getBytes();
                    out.write(data, 0, data.length);
                }
            }
            success = true;
        }
        catch (FileNotFoundException e) {
            ProgrammableThermostat.LOGGER.log(Level.SEVERE, "Files.exists is true, but File not found on read: " + e.getMessage());
            success = false;
        }
        catch (IOException e) {
            ProgrammableThermostat.LOGGER.log(Level.SEVERE, "IO Error saving file: " + e.getMessage());
            success = false;
        }
        catch (Exception e) {
            ProgrammableThermostat.LOGGER.log(Level.SEVERE, e.getMessage());
            success = false;
        }
        return success;
    }
}
