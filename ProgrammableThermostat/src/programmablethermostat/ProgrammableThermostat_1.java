package programmablethermostat;

import java.net.HttpURLConnection;
import java.nio.file.*;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.JsonObject;

/**
 *
 * @author tguar
 */
public class ProgrammableThermostat_1 {

    
    // initiate error logger, protected to use in other files
    protected static final Logger LOGGER = ErrorLogger.initiateLogger();
    // path to settings file
    private static Path settingsPath = Paths.get("thermostat_settings.txt");
    private static Scanner input = new Scanner(System.in);
    
    // main method
    public static void main2(String[] args) {
        
        //OperateScreen.startApp();
        
        
        
        
        /*
            
            // option to delete a program
            System.out.print("Would you like to delete a program? Y/N: ");

            String choice = input.nextLine().substring(0,1);
            boolean deleting = true;
            while(deleting) {
                // user does not want to delete a program
                if(choice.toUpperCase().equals("N")) {
                    System.out.println("Retaining all existing programs.");
                    break;
                }
                // user does want to delete a program
                else if(choice.toUpperCase().equals("Y")) {
                    System.out.println("Enter index of program to delete, 0 to cancel\n");
                    for (int i = 0; i < programList.size(); i++) {
                        ProgrammedSettings setting = programList.get(i);
                        System.out.println("[" + (i + 1) + "] " +
                                setting.getWeekday() + " " + 
                                setting.getStartTime() + "-" + setting.getEndTime() + " " +
                                setting.getTargetTemp() + "\u00B0");
                    }
                    // loop until valid choice
                    boolean looping = true;
                    while (looping) {
                        try {
                            System.out.print("Delete program #: ");
                            int deleteChoice = Integer.parseInt(input.nextLine());
                            // cancel delete operations
                            if(deleteChoice == 0) {
                                deleting = false;
                                break;
                            }
                            // attempt deleting index
                            try {
                                programList.remove(deleteChoice - 1);
                                System.out.println("Program Deleted.");

                                break;
                            }
                            // did not enter a valid index
                            catch(IndexOutOfBoundsException e) {
                                System.out.println("Sorry, that is not a valid choice.");
                            }
                        }
                        // did not enter a number
                        catch (InputMismatchException e) {
                            System.out.println("Sorry, please enter a number");
                        }
                    } // end while looping
                    if(programList.size() > 0) {
                        System.out.print("Would you like to delete another? Y/N: ");
                        choice = input.nextLine().substring(0,1);
                    }
                    else {
                        System.out.println("There are no remaining programmings.");
                        break;
                    }
                } // end if wants to delete program
                else {
                    System.out.print("Please enter Y or N to continue: ");
                    choice = input.nextLine().substring(0,1);
                }
            }
        } // end if-else txt file contains data
        
        // option to create a program
        System.out.print("Would you like to program the thermostat? Y/N: ");
        boolean programming = true;
        while(programming) {
            String choice = input.nextLine().substring(0,1);
            // user does not want to add a program
            if(choice.toUpperCase().equals("N")) {
                programming = false;
                // programs could have been deleted and still needs to be saved
                boolean success = ReadWriteProgramming.saveProgramming(settingsPath, programList);
                if (success) {
                    System.out.println("Settings saved");
                    break;
                }
                else {
                    System.out.println("Error saving settings.");
                    break;
                }
            }
            // user does want to add a program
            else if(choice.toUpperCase().equals("Y")) {
                System.out.println("What day is this program for? (Monday, Tuesday, ...)");
                String weekDay = input.nextLine();
                System.out.println("What time should this program start? (HH:mm)");
                // initialized to compile, progrma will run until valid input
                LocalTime startTime = LocalTime.parse("00:00");
                while(true) {
                    try {
                        startTime = LocalTime.parse(input.nextLine());
                        break;
                    }
                    catch(DateTimeParseException e){
                        System.out.println("Please enter time in format 'HH:MM'");
                    }
                }
                System.out.println("What time should this program end? (HH:mm)");
                // initialized to compile, progrma will run until valid input
                LocalTime endTime = LocalTime.parse("00:00");
                while(true) {
                    try {
                        endTime = LocalTime.parse(input.nextLine());
                        break;
                    }
                    catch(DateTimeParseException e){
                        System.out.println("Please enter time in format 'HH:MM'");
                    }
                }
                System.out.println("What temperature would you like?");
                // initialized at zero, program will run until valid input
                int targetTemp = 0;
                while(true) {
                    try {
                        targetTemp = Integer.parseInt(input.nextLine());
                        break;
                    }
                    catch(NumberFormatException e) {
                        System.out.println("Sorry, please enter a whole number");
                    }
                }
                System.out.println("What is the minimum allowable temperature?");
                // initialized at zero, program will run until valid input
                int minTemp = 0;
                while(true) {
                    try {
                        minTemp = Integer.parseInt(input.nextLine());
                        if(minTemp > targetTemp) {
                            System.out.println("Error: Minimum temperature cannot be higher than target.");
                            System.out.println("Please enter a temperature lower than " + targetTemp);
                        }
                        else {
                            break;
                        }
                    }
                    catch (NumberFormatException e) {
                        System.out.println("Sorry, please enter a whole number");
                    }
                }
                System.out.println("What is the maximum allowable temperature?");
                // initialized at zero, program will run until valid input
                int maxTemp = 0;
                while(true) {
                    try {
                        maxTemp = Integer.parseInt(input.nextLine());
                        if(maxTemp < targetTemp) {
                            System.out.println("Error: Maximum temperature cannot be lower than target.");
                            System.out.println("Please enter a temperature higher than " + targetTemp);
                        }
                        else {
                            break;
                        }
                    }
                    catch (NumberFormatException e) {
                        System.out.println("Sorry, please enter a whole number");
                    }
                }
                programList.add(new ProgrammedSettings(weekDay,
                startTime, endTime, targetTemp, minTemp, maxTemp));
                System.out.println("Program created.");

                System.out.print("Would you like to save another program? Y/N: ");

            } // end else-if user wants to add program
            else {
                System.out.print("Please enter Y or N to continue: ");
            }
        }// end while programming
        input.close();
        
        // Display Updated programming
        programList = ReadWriteProgramming.readProgramming(settingsPath);
        // if file doesn't exist
        if (programList == null) {
            System.out.println("No Thermostat Programming File found.");
        }
        // if file exists and is empty, no programs exist
        else if(programList.isEmpty()) {
            System.out.println("There are no programs saved currently.");
        }
        // if file exists and containts data
        else {
            System.out.println("** Current Program Settings **");
            for (int i = 0; i < programList.size(); i++) {
                ProgrammedSettings setting = programList.get(i);
                System.out.println("Index: [" + (i + 1) + "]");
                System.out.println("Day: " + setting.getWeekday());
                System.out.println("Start: " + setting.getStartTime());
                System.out.println("End: " + setting.getEndTime());
                System.out.println("Target Temp: " + setting.getTargetTemp()  + "\u00B0");
                System.out.println("Min Temp: " + setting.getMinTemp() + "\u00B0");
                System.out.println("Max Temp: " + setting.getMaxTemp() + "\u00B0");
                System.out.println("\n");
            }
        } */
    } 
}
