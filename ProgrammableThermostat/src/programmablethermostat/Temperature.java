package programmablethermostat;

import java.time.LocalDateTime;

/**
 *
 * @author tguar
 * 
 * This class file contains the Temperature Object and getters.
 */
public class Temperature {
    // attributes
    private String identifier;
    private String name;
    private LocalDateTime thermostatTime;
    private LocalDateTime utcTime;
    private double actualTemperatureF;
    private double actualHumidity;
    private int code;
    private String message;
    
    // Constructors
    public Temperature(String identifier, String name, LocalDateTime thermostatTime,
            LocalDateTime utcTime, double actualTemperature, double actualHumidity, int code,
            String message) {
        
        this.identifier = identifier;
        this.name = name;
        this.thermostatTime = thermostatTime;
        this.utcTime = utcTime;
        this.actualTemperatureF = actualTemperature;
        this.actualHumidity = actualHumidity;
        this.code = code;
        this.message = message;
    }
    
    private double fToC(double f) {
        return (f - 32) * (5/9);
    }
    
    public String getIdentifier() {
        return identifier;
    }
    public String getName() {
        return name;
    }
    public LocalDateTime getThermostatTime() {
        return thermostatTime;
    }
    public LocalDateTime getUtcTime() {
        return utcTime;
    }
    public double getActualTemperatureF() {
        return actualTemperatureF;
    }
    public double getActualTemperatureC() {
        return (actualTemperatureF - 32) * (5.0/9.0);
    }
    public double getActualHumidity() {
        return actualHumidity;
    }
    public int getCode() {
        return code;
    }
    public String getMessage() {
        return message;
    }
}
