package programmablethermostat;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;
import java.util.Scanner;
import java.util.logging.Level;
import javax.json.JsonException;
import javax.json.stream.JsonParsingException;
import static programmablethermostat.ProgrammableThermostat.LOGGER;

/**
 *
 * @author tguar
 * 
 * This class file handles all URL connections, including
 *   creating the HttpURLConnection and retrieving JSON String data from URL
 */
public class UrlConnection {
    
    private static URL url = null;
    private static HttpURLConnection connection;
    private static String tempInfo = null;
    
    
    /**
     * Open an HTTP Connection from a URL
     * @param url_for_json String URL to connect to
     * @return HttpURLConnection object
     */
    public static HttpURLConnection makeConnection(String url_for_json) {

        // create URL
        try {
            url = new URL(url_for_json);
        }
        catch(MalformedURLException e){
            ProgrammableThermostat.LOGGER.log(Level.SEVERE, "Incorrect URL: " + e.getMessage());
        }
        catch(Exception e) {
            ProgrammableThermostat.LOGGER.log(Level.WARNING, "Error: " + e.getClass().getName() + " - " + e.getMessage());
        }
        
        if(Objects.nonNull(url)) {
            try {
                // open url connection
                connection = (HttpURLConnection)url.openConnection();
                
                // double check if connection is null
                if(connection == null) {
                    throw new IOException("Connection cannot be null");
                }
                else {
                    // specify request type
                    connection.setRequestMethod("GET");
                    // make connection
                    connection.connect();
                }
                // check http response code
                int responseCode = connection.getResponseCode();
                // if code is 200, log successful connection
                if(responseCode == HttpURLConnection.HTTP_OK) {
                    
                    LOGGER.log(Level.INFO, "HTTP Connection Established");
                }
                else {
                    LOGGER.log(Level.SEVERE, "HTTP Connection Error Code: " + responseCode);
                }
            }
            catch(IOException e) {
                LOGGER.log(Level.SEVERE, "IO Exception: " + e.getMessage());
            }
            catch(Exception e) {
                LOGGER.log(Level.SEVERE, "Error: " + e.getMessage());
            }
        }
        return connection;
    }
    
    /**
     * Read data from connection
     *  trap any known JSON format issues
     * @param connection a valid HttpURLConnection
     * @return String-formatted JSON data
     */
    public static String readJsonData(HttpURLConnection connection) {
        
        
        try(Scanner tempData = new Scanner(connection.getInputStream())) {
            
            // read through data, add to string
            while(tempData.hasNext()) {
            tempInfo += tempData.nextLine();
            }
            // check for "null" as a string at beginning of data
            if(tempInfo.substring(0, 4).equals("null")) {
                // if "null" is present, tempInfo is everything after
                tempInfo = tempInfo.substring(4);
                // check for additional comma - JSON not formatted correctly
                if(tempInfo.substring(227, 230).equals("42,")) {
                    // if incorrect comma found, remove from tempInfo string
                    tempInfo = tempInfo.substring(0, 229) + tempInfo.substring(230);
                }
            }
                
        }
        catch(JsonParsingException e) {
            LOGGER.log(Level.SEVERE, "Incoming JSON formatted incorrectly: " + e.getMessage());
        }
        catch(JsonException e) {
            LOGGER.log(Level.SEVERE, "I/O Error creating JSON object" + e.getMessage());
        }
        catch(IllegalStateException e) {
            LOGGER.log(Level.SEVERE, "Read operation error: " + e.getMessage());
        }
        catch(IOException e) {
            LOGGER.log(Level.SEVERE, "IO Exception: " + e.getMessage());
        }
        return tempInfo;
    }
}
