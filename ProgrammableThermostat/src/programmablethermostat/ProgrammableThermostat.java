package programmablethermostat;

import java.net.HttpURLConnection;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javax.json.JsonObject;

/**
 *
 * @author tguar
 */
public class ProgrammableThermostat extends Application {
    // initiate error logger, protected to use in other files
    protected static final Logger LOGGER = ErrorLogger.initiateLogger();
    
    BorderPane programBorder;
    BorderPane operateBorder;
    @Override
    public void start(Stage primaryStage) {
        programBorder = ProgramScreen.openProgram();
        operateBorder = OperateScreen.operateScene();
        Scene programScene = new Scene(programBorder, 600, 500);
        Scene operateScene = new Scene(operateBorder, 600, 500);
    
        primaryStage.setScene(operateScene);
        
        OperateScreen.programButton.setOnAction(e -> {
            primaryStage.setScene(programScene);
            primaryStage.setTitle("Program");
        });
        ProgramScreen.operateButton.setOnAction(e -> {
            primaryStage.setScene(operateScene);
            primaryStage.setTitle("Operate");
        });
        primaryStage.show();
    }

    
    public static void main(String[] args) {
        launch(args);
    }
    
}
