package programmablethermostat;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import static programmablethermostat.ProgramScreen.timeOfDayReading;
/**
 *
 * @author tguar
 * 
 * This class file contains an Object to store Programmed Temperature settings
 */
public class ProgrammedSettings {
    
    private String weekDay;
    private LocalTime startTime;
    private int targetTemp;
    
    DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("h:00 a");
    
    
    public ProgrammedSettings(String weekDay, LocalTime startTime, int targetTemp) {
        this.weekDay = weekDay;
        this.startTime = startTime;
        this.targetTemp = targetTemp;
    }
    
    public String getWeekday() {
        return weekDay;
    }
    public LocalTime getStartTime() {
        return startTime;
    }
    public int getTargetTemp() {
        return targetTemp;
    }
    
    @Override
    public String toString() {
        return weekDay + " - " + timeFormatter.format(startTime) + " - " + targetTemp;
    }
}
