package programmablethermostat;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 *
 * @author tguar
 */
public class ErrorLogger {
    
    private static Logger logger = Logger.getLogger(ProgrammableThermostat.class.getName());
    
    public static Logger initiateLogger() {
        // console logger
        Logger.getLogger("").getHandlers()[0].setLevel(Level.ALL);
        // custom logger
        logger.setLevel(Level.ALL);
        // Simple Formatter for error log file to make more readable
        // set formatter property
        System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tm/%1$td/%1$tY] %4$s: %5$s %n");
        // create simple formatter object
        SimpleFormatter simpleFormatter = new SimpleFormatter();
        try {
            // Create file handler
            FileHandler fileHandler = new FileHandler("thermostat.console.log");
            // associate formatter to handler
            fileHandler.setFormatter(simpleFormatter);
            // associate filhandler to logger
            logger.addHandler(fileHandler);
        }
        catch(IOException e) {
            // will still log to console
            logger.log(Level.WARNING, "Unable to create log file: " + e.getMessage());
        }
        
        return logger;
    }
            
    
}
