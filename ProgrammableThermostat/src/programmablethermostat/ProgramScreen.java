package programmablethermostat;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.RowConstraints;
import javafx.stage.Stage;

/**
 *
 * @author tguar
 */
public class ProgramScreen {
    
    // path to settings file
    private static Path settingsPath = Paths.get("thermostat_settings.txt");
    
    static final int MAX_TEMP = 90;
    static final int MIN_TEMP = 50;
    static final int DEFAULT_TEMP = 72;
    static int temp;
    static int newTemp;
    static String timeString;
    static LocalTime time;
    static String currentDay;
    static String[] daysOfWeekArray = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
    static int currentDayIndex = 0;
    // re-used styles for temp info
    static String readoutBoxStyle = "-fx-border-colod: black; "
            + "-fx-border-style: solid; "
            + "-fx-background-color: #ffffff; "
            + "-fx-padding: 5";
    static Font readoutFont = new Font("Cambria", 48);
    static DecimalFormat decimalFormat = new DecimalFormat("###.#");
        
    // Border Components
    static BorderPane programBorder = new BorderPane();
    static HBox hboxBorderTitle = new HBox();
    static Label operateTitleLabel = new Label("Program");
    static HBox hboxBorderBottom = new HBox();
    static Label messageLabel = new Label("");
    static VBox leftBorder = new VBox();
    static VBox rightBorder = new VBox();
    
    // Grid Components
    static GridPane grid = new GridPane();
    // Day of the Week
    static VBox dayOfWeekVbox = new VBox();
    static HBox dayOfWeekHbox = new HBox();
    static Label dayOfWeekLabel = new Label("Day of the Week");
    static Label dayOfWeekReading = new Label("Tue");
    static VBox dayControlVbox = new VBox();
    static Button dayUpButton = new Button("\u02C4");
    static Button dayDownButton = new Button("\u02C5");
    // Time of Day
    static VBox timeOfDayVbox = new VBox();
    static HBox timeOfDayHbox = new HBox();
    static Label timeOfDayLabel = new Label("Time of Day");
    static Label timeOfDayReading = new Label("09:00 AM");
    static VBox timeControlVbox = new VBox();
    static Button timeUpButton = new Button("\u02C4");
    static Button timeDownButton = new Button("\u02C5");
    // Current Setting
    static VBox currentSettingVbox = new VBox();
    static HBox currentSettingHbox = new HBox();
    static Label currentSettingLabel = new Label("Current Setting");
    static Label currentSettingReading = new Label("72");
    static VBox tempControlVbox = new VBox();
    static Button tempUpButton = new Button("\u02C4");
    static Button tempDownButton = new Button("\u02C5");
    // Existing Programs
    static VBox programVbox = new VBox();
    static Label programLabel = new Label("Saved Programs");
    static ListView programListView = new ListView();  
    static Label statusLabel = new Label("");
    static Button deleteButton = new Button("Delete Program");
    // Save / Program Buttons
    static Button saveButton = new Button("Save");
    static Button operateButton = new Button("Operate");
    
    
    public static BorderPane openProgram() {
        
        /*
            BorderPane for border and "Operate" title
        */
        hboxBorderTitle.getChildren().addAll(operateTitleLabel);
        hboxBorderTitle.setStyle("-fx-border-color: black; "
                + "-fx-border-style: hidden hidden solid hidden");
        programBorder.setTop(hboxBorderTitle);
        
        hboxBorderBottom.setStyle("-fx-border-color: black; "
                + "-fx-border-style: solid hidden hidden hidden");
        hboxBorderBottom.setMinHeight(22);
        hboxBorderBottom.getChildren().addAll(messageLabel);
        programBorder.setBottom(hboxBorderBottom);
        leftBorder.setPrefWidth(20);
        programBorder.setLeft(leftBorder);
        rightBorder.setPrefWidth(20);
        programBorder.setRight(rightBorder);
        programBorder.setCenter(grid);
        
        /*
            GridPane for main content
        */
        // Grid constrains/sizes
        RowConstraints row0 = new RowConstraints();
        RowConstraints row1 = new RowConstraints();
        RowConstraints row2 = new RowConstraints();
        RowConstraints row3 = new RowConstraints();
        row0.setPercentHeight(25);
        row1.setPercentHeight(25);
        row2.setPercentHeight(25);
        row3.setPercentHeight(25);
        ColumnConstraints col0 = new ColumnConstraints();
        ColumnConstraints col1 = new ColumnConstraints();
        ColumnConstraints col2 = new ColumnConstraints();
        ColumnConstraints col3 = new ColumnConstraints();
        col0.setMinWidth(200);
        col1.setMinWidth(100);
        col2.setMinWidth(100);
        col3.setMinWidth(200);
        grid.getRowConstraints().addAll(row0, row1, row2, row3);
        grid.getColumnConstraints().addAll(col0, col1, col2, col3);
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(1);
        grid.setVgap(1);
        
        
        /*
        Place Nodes in grid
        */
        // Day of week
        dayOfWeekReading.setStyle(readoutBoxStyle);
        dayOfWeekReading.setFont(readoutFont);
        dayOfWeekReading.setMinWidth(110);
        currentDay = daysOfWeekArray[currentDayIndex];
        dayOfWeekReading.setText(currentDay);
        dayControlVbox.getChildren().addAll(dayUpButton, dayDownButton);
        dayControlVbox.setStyle("-fx-padding: 5 0 0 10");
        dayOfWeekHbox.getChildren().addAll(dayOfWeekReading, dayControlVbox);
        dayOfWeekVbox.getChildren().addAll(dayOfWeekLabel, dayOfWeekHbox);
        grid.add(dayOfWeekVbox, 0, 0, 2, 1);
        // Time of Day
        timeOfDayReading.setStyle(readoutBoxStyle);
        timeOfDayReading.setFont(readoutFont);
        timeOfDayReading.setMinWidth(225);
        timeControlVbox.getChildren().addAll(timeUpButton, timeDownButton);
        timeControlVbox.setStyle("-fx-padding: 5 0 0 10");
        timeOfDayHbox.getChildren().addAll(timeOfDayReading, timeControlVbox);
        timeOfDayVbox.getChildren().addAll(timeOfDayLabel, timeOfDayHbox);
        grid.add(timeOfDayVbox, 0, 1, 2, 1);
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("h:00 a");
        timeOfDayReading.setText(timeFormatter.format(LocalTime.now()));
        // Temp Setting
        currentSettingReading.setStyle(readoutBoxStyle);
        currentSettingReading.setFont(readoutFont);
        tempControlVbox.getChildren().addAll(tempUpButton, tempDownButton);
        tempControlVbox.setStyle("-fx-padding: 5 0 0 10");
        currentSettingHbox.getChildren().addAll(currentSettingReading, tempControlVbox);
        currentSettingVbox.getChildren().addAll(currentSettingLabel, currentSettingHbox);
        grid.add(currentSettingVbox, 0, 2, 2, 1);
        // Program Settings
        programListView.setEditable(true);
        programListView.setMaxSize(200, 250);
        programVbox.getChildren().addAll(programLabel, programListView, statusLabel, deleteButton);
        grid.add(programVbox, 2, 0, 2, 3);
        GridPane.setHalignment(programVbox, HPos.LEFT);
        GridPane.setValignment(programVbox, VPos.TOP);
        // Save Button
        grid.add(saveButton, 1, 3);
        grid.add(operateButton, 2, 3);
        
        // Retrieve current saved programming
        ArrayList<ProgrammedSettings> programList = ReadWriteProgramming.readProgramming(settingsPath);
        // if file doesn't exist
        if (programList == null) {
            statusLabel.setText("No Thermostat Programming File found.");
        }
        // if file exists and is empty, no programs exist
        else if(programList.isEmpty()) {
            statusLabel.setText("There are no programs saved currently.");
        }
        // if file exists and containts data
        else {
            statusLabel.setText("programs found");
            programList.forEach((e) -> {
                programListView.getItems().add(e);
            });
        }
        
        /*
            Day Adjustment Event Handler
        */
        dayUpButton.setOnAction(e -> {
            if (currentDayIndex == daysOfWeekArray.length - 1) {
                currentDayIndex = 0;
            }
            else {
                currentDayIndex = currentDayIndex + 1;
            }
            dayOfWeekReading.setText(daysOfWeekArray[currentDayIndex]);
            
        });
        dayDownButton.setOnAction(e -> {
            if (currentDayIndex == 0) {
                currentDayIndex = daysOfWeekArray.length - 1;
            }
            else {
                currentDayIndex = currentDayIndex - 1;
            }
            dayOfWeekReading.setText(daysOfWeekArray[currentDayIndex]);
        });
        /*
            Time Adjustment Event Handler
        */
        timeUpButton.setOnAction(e -> {
            timeString = timeOfDayReading.getText();
            time = LocalTime.parse(timeString, timeFormatter);
            time = time.plusHours(1);
            timeOfDayReading.setText(timeFormatter.format(time));
        });
        timeDownButton.setOnAction(e -> {
            timeString = timeOfDayReading.getText();
            time = LocalTime.parse(timeString, timeFormatter);
            time = time.minusHours(1);
            timeOfDayReading.setText(timeFormatter.format(time));
        });
        /*
            Temp Adjustment Event Handler
        */
        tempUpButton.setOnAction(e -> {
            temp = Integer.parseInt(currentSettingReading.getText());
            // can not go above MAx_TEMP
            if (temp + 1 > MAX_TEMP) {
                messageLabel.setText("Err: Temp setting can not be higher than " + MAX_TEMP);
            }
            else {
                newTemp = temp + 1;
                currentSettingReading.setText(Integer.toString(newTemp));
                messageLabel.setText("");
            }
        });
        tempDownButton.setOnAction(e -> {
            temp = Integer.parseInt(currentSettingReading.getText());
            // can not go below MIN_TEMP
            if (temp - 1 < MIN_TEMP) {
                messageLabel.setText("Err: Temp setting can not be lower than " + MIN_TEMP);
            }
            else {
                newTemp = temp - 1;
                currentSettingReading.setText(Integer.toString(newTemp));
                messageLabel.setText("");
            }
        });
        /*
            Save Button Event Handler
        */
        saveButton.setOnAction(e -> {
            String weekDay = dayOfWeekReading.getText();
            timeString = timeOfDayReading.getText();
            time = LocalTime.parse(timeString, timeFormatter);
            temp = Integer.parseInt(currentSettingReading.getText());
            programList.add(new ProgrammedSettings(weekDay, time, temp));
            statusLabel.setText("");
            programListView.getItems().clear();
            programList.forEach((p) -> {
                programListView.getItems().add(p);
            });
            boolean success = ReadWriteProgramming.saveProgramming(settingsPath, programList);
            if (success) {
                statusLabel.setText("Programming Saved");
            }
            else {
                statusLabel.setText("Error saving programming");
            }
        });
        
        /*
            Delete Button Event Handler
        */
        deleteButton.setOnAction(e -> {
            int selectedIdx = programListView.getSelectionModel().getSelectedIndex();
            programListView.getItems().remove(selectedIdx);
            programList.remove(selectedIdx);
            boolean success = ReadWriteProgramming.saveProgramming(settingsPath, programList);
            if (success) {
                statusLabel.setText("Program Deleted");
            }
            else {
                statusLabel.setText("Error deleting programming");
            }
        });
        
        /*
            Operate Button Event Handler
        */
        // Handled in main
        
        
        return programBorder;
    }
    
    
    
}
