package programmablethermostat;


import java.net.HttpURLConnection;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javax.json.JsonObject;
import static programmablethermostat.ProgrammableThermostat.LOGGER;

/**
 *
 * @author tguar
 */
public class OperateScreen{
    
    // url to JSON data
    private static final String URL_FOR_JSON = "http://media.capella.edu/BBCourse_Production/IT4774/temperature.json";
    static final int MAX_TEMP = 90;
    static final int MIN_TEMP = 50;
    static final int DEFAULT_TEMP = 72;
    static public int temp;
    static public int newTemp;
    static String timeString;
    static String currentTemp;
    static String currentHumidity;
    
    // re-used styles for temp info
    static String readoutBoxStyle = "-fx-border-colod: black; "
            + "-fx-border-style: solid; "
            + "-fx-background-color: #ffffff; "
            + "-fx-padding: 5";
    static Font readoutFont = new Font("Cambria", 48);
    static DecimalFormat decimalFormat = new DecimalFormat("###.#");
        
    // Border Components
    static BorderPane operateBorder = new BorderPane();
    static HBox hboxBorderTitle = new HBox();
    static Label operateTitleLabel = new Label("Operate");
    static HBox hboxBorderBottom = new HBox();
    static Label messageLabel = new Label("");
    static VBox leftBorder = new VBox();
    static VBox rightBorder = new VBox();
    
    // Grid Components
    static GridPane grid = new GridPane();
    // Current Inside Temperature
    static VBox currentInsideTempVbox = new VBox();
    static HBox temperatureHbox = new HBox();
    static Label currentInsideTempLabel = new Label("Current Inside Temperature");
    static Label currentInsideTempReading = new Label("999.9");
    // Current Inside Relative Humidity
    static VBox currentHumidityVbox = new VBox();
    static Label currentHumidityLabel = new Label("Current Inside Relative Humidity");
    static Label currentHumidityReading = new Label("99.9%");
    // Current Temperature Setting
    static VBox currentSettingVbox = new VBox();
    static HBox tempControlHbox = new HBox();
    static Label currentSettingLabel = new Label("Current Setting");
    static Label currentSettingReading = new Label("72");
    // Current Time
    static VBox currentTimeVbox = new VBox();
    static Label currentTimeLabel = new Label("Current Time");
    static Label currentTimeReading = new Label("00:00");
    // Buttons for Fahrenheit / Celsius
    static VBox tempMetricVbox = new VBox();
    static ToggleGroup metricButtonGroup = new ToggleGroup();
    static RadioButton celsiusRButton = new RadioButton("Celsius");
    static RadioButton fahrenheitRButton = new RadioButton("Fahrenheit");
    // Buttons for temp up / temp down
    static VBox tempControlVbox = new VBox();
    static Button tempUpButton = new Button("\u02C4");
    static Button tempDownButton = new Button("\u02C5");
    // Fan Button
    static VBox fanVbox = new VBox();
    static Button fanButton = new Button("Fan");
    static Label fanLabel = new Label("Auto");
    // System Button
    static VBox systemVbox = new VBox();
    static Button systemButton = new Button("System");
    static Label systemLabel = new Label("Cool");
    // Reset Button
    static Button resetButton = new Button("Default");
    // Program Button
    static Button programButton = new Button("Program");
    
    
    public static BorderPane operateScene() {
        
        Temperature temperature;
        
        
        /*
            BorderPane for border and "Operate" title
        */
        hboxBorderTitle.getChildren().addAll(operateTitleLabel);
        hboxBorderTitle.setStyle("-fx-border-color: black; "
                + "-fx-border-style: hidden hidden solid hidden");
        operateBorder.setTop(hboxBorderTitle);
        
        hboxBorderBottom.setStyle("-fx-border-color: black; "
                + "-fx-border-style: solid hidden hidden hidden");
        hboxBorderBottom.setMinHeight(22);
        hboxBorderBottom.getChildren().addAll(messageLabel);
        operateBorder.setBottom(hboxBorderBottom);
        leftBorder.setPrefWidth(20);
        operateBorder.setLeft(leftBorder);
        rightBorder.setPrefWidth(20);
        operateBorder.setRight(rightBorder);
        operateBorder.setCenter(grid);
        
        
        /*
            GridPane for main content
        */
        // Grid constrains/sizes
        RowConstraints row0 = new RowConstraints();
        RowConstraints row1 = new RowConstraints();
        RowConstraints row2 = new RowConstraints();
        RowConstraints row3 = new RowConstraints();
        row0.setPercentHeight(25);
        row1.setPercentHeight(25);
        row2.setPercentHeight(25);
        row3.setPercentHeight(25);
        ColumnConstraints col0 = new ColumnConstraints();
        ColumnConstraints col1 = new ColumnConstraints();
        ColumnConstraints col2 = new ColumnConstraints();
        col0.setMinWidth(250);
        col1.setMinWidth(100);
        col2.setMinWidth(200);
        grid.getRowConstraints().addAll(row0, row1, row2, row3);
        grid.getColumnConstraints().addAll(col0, col1, col2);
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(1);
        grid.setVgap(1);
        
        
        
        /*
        Place Nodes in grid
        */
        // Current Inside Temp - Label, Readout, F/C buttons
        fahrenheitRButton.setToggleGroup(metricButtonGroup);
        fahrenheitRButton.setSelected(true);
        celsiusRButton.setToggleGroup(metricButtonGroup);
        tempMetricVbox.getChildren().addAll(fahrenheitRButton, celsiusRButton);
        currentInsideTempReading.setStyle(readoutBoxStyle);
        currentInsideTempReading.setFont(readoutFont);
        temperatureHbox.getChildren().addAll(currentInsideTempReading, tempMetricVbox);
        tempMetricVbox.setStyle("-fx-padding: 10");
        currentInsideTempVbox.getChildren().addAll(
                currentInsideTempLabel, temperatureHbox);
        grid.add(currentInsideTempVbox, 0, 0);
        GridPane.setHalignment(currentInsideTempVbox, HPos.LEFT);
        
        // Humidity - Label, Readout
        currentHumidityReading.setStyle(readoutBoxStyle);
        currentHumidityReading.setFont(readoutFont);
        currentHumidityVbox.getChildren().addAll(
                currentHumidityLabel, currentHumidityReading);
        grid.add(currentHumidityVbox, 0, 1);
        GridPane.setHalignment(currentHumidityVbox, HPos.LEFT);
        
        // Current Temp Setting - Label, Reaout, Adjustment buttons
        tempControlVbox.getChildren().addAll(tempUpButton, tempDownButton);
        currentSettingReading.setStyle(readoutBoxStyle);
        currentSettingReading.setFont(readoutFont);
        tempControlHbox.getChildren().addAll(currentSettingReading, tempControlVbox);
        currentSettingVbox.getChildren().addAll(
                currentSettingLabel, tempControlHbox);
        tempControlVbox.setStyle("-fx-padding: 5 0 0 10");
        grid.add(currentSettingVbox, 0, 2);
        GridPane.setHalignment(currentSettingVbox, HPos.LEFT);
        
        // Fan - Label, Button
        fanVbox.getChildren().addAll(fanButton, fanLabel);
        fanLabel.setAlignment(Pos.CENTER);
        grid.add(fanVbox, 2, 0);
        fanVbox.setAlignment(Pos.CENTER_RIGHT);
        
        // System - Label, Button
        systemVbox.getChildren().addAll(systemButton, systemLabel);
        grid.add(systemVbox, 2, 1);
        systemVbox.setAlignment(Pos.CENTER_RIGHT);
        
        // Time - Label, Button
        currentTimeReading.setStyle(readoutBoxStyle);
        currentTimeReading.setFont(readoutFont);
        currentTimeVbox.getChildren().addAll(
                currentTimeLabel, currentTimeReading);
        grid.add(currentTimeVbox, 2, 2);
        currentTimeVbox.setAlignment(Pos.CENTER);
        
        // Reset - Button
        grid.add(resetButton, 0, 3);
        GridPane.setHalignment(resetButton, HPos.LEFT);
        resetButton.setStyle("-fx-border-color: black; "
                + "-fx-background-color: RED");
        
        // Program - Button
        grid.add(programButton, 1, 3);
        GridPane.setHalignment(programButton, HPos.LEFT);
        
        
        
        
        /*
            Load Data from JSON API into UI
        */
        // Create and open connection
        HttpURLConnection connection = UrlConnection.makeConnection(URL_FOR_JSON);
        // check that connection is not null
        if(connection != null) {
            
            // Retrieve data string from URL
            String tempData = UrlConnection.readJsonData(connection);
            // Format String into JSON
            JsonObject jsonTemperature = TemperatureParser.stringToJsonObject(tempData);
            // Format JSON into Temperature Object
            temperature = TemperatureParser.jsonToTemperatureObject(jsonTemperature);
            
            // if temperature is not null, print data to console
            if(temperature != null) {
                LOGGER.log(Level.INFO, "Writing Temperature Data");
                
                // Brief delay so logger doesn't print in middle of data output
                try {
                    Thread.sleep(250);
                }
                catch(InterruptedException e) {
                    LOGGER.log(Level.INFO, "Sleep delay interrupted: " + e.getMessage());
                }
                
                DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("h:mm a");
                timeString = timeFormatter.format(temperature.getThermostatTime());
                currentTimeReading.setText(timeString);
                
                currentTemp = decimalFormat.format(temperature.getActualTemperatureF());
                currentInsideTempReading.setText(currentTemp);
                
                currentHumidity = decimalFormat.format(temperature.getActualHumidity());
                currentHumidityReading.setText(currentHumidity + "%");
                
                currentSettingReading.setText(Integer.toString(DEFAULT_TEMP));
                
                messageLabel.setText(temperature.getMessage());

                /*
                    Reset Button event handler
                */
                resetButton.setOnAction(e -> {
                    messageLabel.setText("Reset to default settings");
                    
                    timeString = timeFormatter.format(temperature.getThermostatTime());
                    currentTimeReading.setText(timeString);

                    currentTemp = decimalFormat.format(temperature.getActualTemperatureF());
                    currentInsideTempReading.setText(currentTemp);

                    currentHumidity = decimalFormat.format(temperature.getActualHumidity());
                    currentHumidityReading.setText(currentHumidity + "%");

                    currentSettingReading.setText(Integer.toString(DEFAULT_TEMP));
                    
                    fahrenheitRButton.setSelected(true);
                    
                });
                /*
                    Fahrenheit/Celsius Event Handler
                */
                metricButtonGroup.selectedToggleProperty().addListener((obs, oldBtn, newBtn) -> {
                    if (celsiusRButton.isSelected()) {
                        currentInsideTempReading.setText(decimalFormat.format(temperature.getActualTemperatureC()));
                    }
                    else if (fahrenheitRButton.isSelected()) {
                        currentInsideTempReading.setText(decimalFormat.format(temperature.getActualTemperatureF()));
                    }
                });
                /*
                    Temperature Adjustment Event Handler
                */
                tempUpButton.setOnAction(e -> {
                    temp = Integer.parseInt(currentSettingReading.getText());
                    // can not go above MAx_TEMP
                    if (temp + 1 > MAX_TEMP) {
                        messageLabel.setText("Err: Temp setting can not be higher than " + MAX_TEMP);
                    }
                    else {
                        newTemp = temp + 1;
                        currentSettingReading.setText(Integer.toString(newTemp));
                        messageLabel.setText("");
                    }
                });
                tempDownButton.setOnAction(e -> {
                    temp = Integer.parseInt(currentSettingReading.getText());
                    // can not go below MIN_TEMP
                    if (temp - 1 < MIN_TEMP) {
                        messageLabel.setText("Err: Temp setting can not be lower than " + MIN_TEMP);
                    }
                    else {
                        newTemp = temp - 1;
                        currentSettingReading.setText(Integer.toString(newTemp));
                        messageLabel.setText("");
                    }
                });
                
                /*
                    Program Button Event Handler
                */
                // Handled in main
            }
        }
        
        
        return operateBorder;
    }
}
