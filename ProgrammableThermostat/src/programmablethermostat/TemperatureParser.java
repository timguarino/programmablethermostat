package programmablethermostat;

import java.io.StringReader;
import java.time.LocalDateTime;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
/**
 *
 * @author tguar
 * 
 * This class file contains the method to
 *   convert a JSON string into a Java Temperature Object
 */
public class TemperatureParser {
    
    /**
     * Converts String into JSON
     * @param jsonInput String from URL response
     * @return JSON object
     */
    public static JsonObject stringToJsonObject(String jsonInput){
        
        // StringReader to pass into JsonReader
        StringReader stringReader = new StringReader(jsonInput);
        // Declare the JSON data Object
        JsonObject jsonTempData;
        // try-with-resources to read data and close reader
        try(JsonReader jsonReader = Json.createReader(stringReader)) {
            // top-level JSON Object
            jsonTempData = jsonReader.readObject();
        }
        
        return jsonTempData;
    }
    
    
    /**
     * Translate JSON into Java Temperature Object
     * @param jsonTempData JsonObject of temperature data
     * @return Temperature Object
     */
    public static Temperature jsonToTemperatureObject(JsonObject jsonTempData) {
        
        // Data from top-level
        String identifier = jsonTempData.getString("identifier");
        String name = jsonTempData.getString("name");
        String thermostatTimeString = jsonTempData.getString("thermostatTime");
        thermostatTimeString = thermostatTimeString.substring(0,10) + "T" + thermostatTimeString.substring(11);
        LocalDateTime thermostatTime = LocalDateTime.parse(thermostatTimeString);
        String utcTimeString = jsonTempData.getString("utcTime");
        utcTimeString = utcTimeString.substring(0,10) + "T" + utcTimeString.substring(11);
        LocalDateTime utcTime = LocalDateTime.parse(utcTimeString);
        
        // Second level JSON Object with live temp data
        JsonObject runtime = jsonTempData.getJsonObject("runtime");
        double actualTemperature = runtime.getJsonNumber("actualTemperature").doubleValue();
        if (actualTemperature == 711) {
            actualTemperature = 71.1;
        }
        double actualHumidity = runtime.getJsonNumber("actualHumidity").doubleValue();
        
        // Second level JSON Object with information
        JsonObject status = jsonTempData.getJsonObject("status");
        int code = status.getInt("code");
        String message = status.getString("message");
        
        // Create new Temperature object from input
        Temperature temperature = new Temperature(identifier, name, thermostatTime,
                utcTime, actualTemperature, actualHumidity, code, message);
        
        return temperature;
    }
}
